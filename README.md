
Hologram Maker for 3ds Max


* Create stunning and easy videos in 3ds max to show in hologram pyramids for iPad, iPhone and Android Devices
* Version 0.0.1
* Visit or contact us in www.holomaker.com

Set-Up

* To start, you need a full version 3ds max (2012,13,14,15,16)
* This code works with Maxscript, the code editor inside 3ds Max
* You can make and hologram from any animation you have
* The animation must contain only ONE object in the scene (it can be a simple object or a character)
* Open Maxscript and drag the code to the code Editor
* Press Control+E to Run it

HoloMaker Layout Box

* The Code works with the simple parameteres:

* Pick Object (Select the object you want to create an hologram)
* Camera Distance
* Camera Rotation
* Camera Height
* Devices (You can select the render resolution from which device you want to object the Hologram)